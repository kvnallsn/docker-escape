#!/bin/sh

: "${NEW_USERNAME:?"Need to set username via docker run -e}"
: "${NEW_PASSWORD:?"Need to set password via docker run -e}"

cp /chroot-script.sh /host/chroot-script.sh
chroot /host ./chroot-script.sh $NEW_USERNAME $NEW_PASSWORD
